/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package httpmirror;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import javax.activation.MimetypesFileTypeMap;

/**
 *
 * @author Oliver Daus
 */
public class HTTPMirror{
    
    public static final String appName = "HTTPMirror";
    public static final String appVersion = "0.8.4c";
    public static final String appCopyright = "Oliver Daus (c) 2012";
    public static final String appFullVersionString = appName + " v" + appVersion + "   " + appCopyright;
    
    public static boolean cmdDebugMode = false;
    
    BufferedReader in; 
    MirrorServer server;
    
    public HTTPMirror(){
        init();
    }
    
    public static void main(String[] args) {
    
        System.out.println("Commnadline args: ");
        
        for(String arg: args){
            if (arg.equals("-debug")){
                System.out.println("Debug Mode enabled.");
                cmdDebugMode = true;
            }
        }
    
        System.out.println("\n");
        
        @SuppressWarnings("unused")
		HTTPMirror mirror = new HTTPMirror();
    }
    
    public void init() {
	
    	Console console = new Console();
    	
    	while (true) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {}
			
			if (server != null && server.waitsForTermination() && server.hasStoped){
				server = null;
		    	System.out.println("Terminating Mirrorserver ... [ OK ]");
			}
		}
	}
    
    public boolean loadMIMEType(MimetypesFileTypeMap mime){
    	
    	BufferedReader reader = null;
    	try {
    		reader = new BufferedReader(new FileReader(new File("mime.types")));
        	
			while (reader.ready()) {
				String currline = reader.readLine().trim();
				if (currline.contains("#")) currline = currline.substring(0, currline.indexOf("#")).trim();
				if (!currline.isEmpty()){
					mime.addMimeTypes(currline);
				}
			}
		} catch (Exception e) {
			System.out.println("Error: can't load MIME Types: " + e);
			e.printStackTrace();
		}finally{
			if (reader != null){
				try {
					reader.close();
				} catch (Exception e) {}
			}
		}
    	
    	return false;
    }
    
    public class Console extends Thread{
    	
    	private boolean run = true;
    	private MimetypesFileTypeMap mime = new MimetypesFileTypeMap();
    	
    	public Console() {
    		this.start();
		}
    	
		public MimetypesFileTypeMap getMime() {
			return mime;
		}

		public void setMime(MimetypesFileTypeMap mime) {
			this.mime = mime;
		}

		public void run(){
			in = new BufferedReader(new InputStreamReader(System.in));
			
		    System.out.println(appFullVersionString);
		    System.out.println("Enter \"help\" for help.");
		    System.out.println("");
		    
		    //System.out.println("start 80");
		    //checkCommand("start 80");
		    while(run){       
		    	checkCommand(readLine());
		    }
		} 
		
		public void terminate() {
			run = false;
		}
		
		private void startServer(int port){
		    if (server == null || (server != null && server.hasStoped())){
		        //new server
		        server = new MirrorServer(appName +" "+ appVersion, port);
		
		        mime = new MimetypesFileTypeMap();
		        loadMIMEType(mime);
		        server.setMime(mime);
		        
		        //start Thread ...
		        System.out.println("");
		        System.out.print("Starting Mirrorserver on port " + port + " ");
		
		        server.start();
		        
		        while(!server.isRunning() && !server.hasStopedWithError()){
		        	printAnimatedDots();
		        }
		        String error = "";
		        if (server.getError() != null) error = server.getError().getMessage();
		        printAnimatedDotsOKorError(server.hasStopedWithError(), error);
		        if (server.hasStopedWithError()) server = null;
		        
			}else{
				System.out.println("The mirrorserver is already running on port " + server.getServerPort() + " ...");
			}
		    System.out.println("");
		}
		
		public void stopServer() {
			stopServer(false);
		}
		
		public void stopServer(boolean stopSave) {
			if (server != null){
		    	//stop ...
		        System.out.print("Terminating Mirrorserver ");
		                
		        server.terminate(stopSave);
		
		        if (!stopSave){
			        while(!server.hasStoped()){
			           printAnimatedDots();
			        }
			        System.out.println("... [ OK ]");
			        server = null;
		        }else{
		        	System.out.print("...\n");
		        }
			}else{
				System.out.println("No mirrorserver is running ...");
			}
			System.out.println("");
		}
		
		public void printAnimatedDots(){
			 try {
		         Thread.sleep(200);
		         System.out.print(".");
		         Thread.sleep(200);
		         System.out.print(".");
		         Thread.sleep(200);
		         System.out.print(".");
		         Thread.sleep(200);
		         System.out.print("\b\b\b   \b\b\b");
		     } catch (Exception e) {
		     }
		}
		
		public void printAnimatedDotsOK(){
			 System.out.println("... [ OK ]");
		}
		
		public void printAnimatedDotsOKorError(boolean error, String errorMsg){
			if (error){
		        System.out.println("... [ ERROR ]");
		        System.out.println(errorMsg);
		    }else{
		        System.out.println("... [ OK ]");
		    }
		}
		
	    public String readLine(){
	    	try {
				return in.readLine();
			} catch (Exception e) {}
	    	
	    	return "";
	    }
		
		private void checkCommand(String commndString){
			
			//split args ...
			String cmd = commndString;
			String[] args = null;
			
			if (commndString.contains(" ")){
		        cmd = commndString.split(" ")[0];
		
		        String argsString = commndString.substring(commndString.indexOf(" ") + 1);
		
		        if (argsString.contains(" ")){
		            args = argsString.split(" ");
		        }else{
		            args = new String[1];
		            args[0] = argsString;
		        }
			}
		
		//    	=====================================================================================
		//    	==  commands  =======================================================================
		//    	=====================================================================================
			
			//usage / help
			String usage_start = "  usage: start [port]  Starts the mirrorserver. defaultport: 80\n                port   A Portnummber";
			String usage_stop = "  usage: stop [save]   Stops the mirrorserver\n               save    Wait for closure of all connections";
			String usage_exit = "  usage: exit   Quits " + appName + " v" + appVersion;
			    	
			//command: start port
		    if (cmd.equalsIgnoreCase("start")){
				
		        if (args != null && args.length == 1){
		            try {
		                int arg = Integer.valueOf(args[0]);
		                startServer(arg);
		
		            } catch (Exception e) {
		                System.out.println(usage_start);
		            }
		        }else{
		        	startServer(80);
		        }
			}
			
		    //command: stop
		    if (cmd.equalsIgnoreCase("stop")){
		
		        if (args == null){
		            //stop ...
		            stopServer();
		        }else if (args != null && args.length == 1 && args[0].equalsIgnoreCase("save")){
		        	stopServer(true);
		        }else{
		            System.out.println(usage_stop);
		        }
			}
					
		    //command: exit
		    if (cmd.equalsIgnoreCase("exit")){
		
		        if (args == null){
		        System.out.println("");
		        System.out.println("Exiting ...");
		
		        //stop ...
		        if (server != null) stopServer();
		
		            try {
		                Thread.sleep(1000);
		            } catch (InterruptedException e) {
		            }
		
		            //exit
		            run = false;
		            System.exit(0);
		        }else{
		            System.out.println(usage_exit);
		        }
			}
		    
		    //command: debug
		    if (cmd.equalsIgnoreCase("debug")){
		    	//debug
		        if (args == null){
		        	if (server != null){ 
			        	server.debug = !server.debug;
			        	if (server.debug) System.out.println("Debug on");
			        	if (! server.debug) System.out.println("Debug off");
		        	}else{
		        		System.out.println("No mirrorserver is running ...");
		        	}
		        }
			}
			
		    //command: help
		    if (cmd.equalsIgnoreCase("help")){
		        //help
		        System.out.println(appFullVersionString);
		        System.out.println("");
		        System.out.println("available commands: ");
		        System.out.println("");
		        System.out.println("  help          This help screen");
		        System.out.println("  start [port]  Starts the mirrorserver. defaultport: 80");
		        System.out.println("        port    A Portnummber");
		        System.out.println("  stop [save]   Stops the mirrorserver");
		        System.out.println("        save    Wait for closure of all connections");
		        //new 0.9.0
		        System.out.println("  debug         This enables the debug mode");
		        System.out.println("  exit          Quits " + appName + " v" + appVersion);
		        System.out.println("");
			}
		
		}
		
	}
}
