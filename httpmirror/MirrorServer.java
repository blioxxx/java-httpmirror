package httpmirror;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import javax.activation.MimetypesFileTypeMap;


/**
 *
 * @author Oliver Daus
 */
public class MirrorServer extends Thread{

	public boolean debug = false;
	public boolean stopSave = false;
	
    private InputStream in;
    private PrintWriter out;
    
	private String serverName = "";
	private String serverAddress = "";
    private int serverPort;
    private MimetypesFileTypeMap mime = new MimetypesFileTypeMap();
    
    private boolean run = true;
    private boolean terminateSave = false;
    public boolean hasStoped = false;
    private boolean isRunning = false;
    private boolean stopedWithError = false;
    private Exception error;

    ServerSocket serversocket;
    Socket clientsocket;
    
    ArrayList<Client> clients = new ArrayList<Client>();
    private int views = 0;
    
    public MirrorServer(String name, int port){
        this.serverName = name;
    	this.serverPort = port;
    }
    
    public MimetypesFileTypeMap getMime() {
		return mime;
	}

	public void setMime(MimetypesFileTypeMap mime) {
		this.mime = mime;
	}

	@Override
    public void run(){
        try{   
            runServer();
        } catch (Exception ex){
            System.err.println(ex);
        }
    }
    
    public void runServer(){
        try {
        	ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serversocket = serverSocketChannel.socket(); //new ServerSocket(port);
            
            serversocket.bind(new InetSocketAddress(serverPort)); 
            //serversocket.setSoTimeout(1000);
            
            while(run){
                isRunning = true;
                hasStoped = false;            

                clientsocket = null;

                try{
                    if (run) clientsocket = serversocket.accept();
                } catch (Exception ex){
                    //System.out.println(ex);
                }

                if (clientsocket != null){
                	serverAddress = clientsocket.getInetAddress().getCanonicalHostName();
                    Client client = new Client(this, clientsocket);
                    client.start();
                    
                }
                
            }   
            
            while(clients.size() > 0 && terminateSave){
            	Thread.sleep(1000);
            }
            
            for(Client client: clients){
            	client.clientsocket.close();
            }
            
            serversocket.close();
            
        }catch (Exception e) {
            this.error = e;
            stopedWithError = true;
        }

        hasStoped = true;
        isRunning = false;
    }
    
    @Override
    public void start(){
        super.start();
        run = true;
    }

    public void terminate(){
       terminate(false);
    }
    
    public boolean waitsForTermination(){
    	return terminateSave;
    }
    
    public void terminate(boolean terminateSave){
        run = false;
        this.terminateSave = terminateSave;
        try {
			serversocket.close();
		} catch (Exception e) {}
    }
    
    public boolean isRunning(){
        return isRunning;
    }
    
    public boolean hasStoped(){
        return hasStoped;
    }
    
    public boolean hasStopedWithError(){
        return stopedWithError;
    }
    
    public Exception getError(){
        return error;
    }
    
    public String getServerName(){
    	return serverName;
    }
    
    public String getServerAddress(){
    	return serverAddress;
    }
    
    public int getServerPort(){
    	return serverPort;
    }
    
    public int getCurrentConnectionsCount(){
        return clients.size();
    }
    
    public void updateViews(){
        views++;
    }
    public int getViews(){
        return views;
    }
    
    class Client extends Thread{
        
        Socket clientsocket;
        MirrorServer server;
        
        private Exception error;
        
        public Client(MirrorServer server, Socket client){
            this.server = server;
            this.clientsocket = client;
            
            server.clients.add(this);
        }
        
        private String readLine(InputStream in){
             String line = "";
             
             try{
				 while (!line.endsWith("\n")){ 
			    	   line += (char)in.read();
			     } 
             }catch(Exception ex){
             }

             return line.trim();
        }
        
        @Override
        public void run(){
            try {
                server.updateViews();
                
                if (clientsocket != null){
                    views++;

                    String connection = clientsocket.getInetAddress() + " from port " + clientsocket.getPort();
                    System.out.println("connection #" + server.getViews() +  ": " + connection);

                    in = clientsocket.getInputStream();

                    String inStream = "";

                    String command = "";
                    String querry = "";
                    String protocol = "";
                    String protocolVersion = "";
                    String host = "";
                    int urlPort = 80;
                    
                    boolean post = false;
                    int contentlenght = 0;
                    String contenType = "";
                    String boundary = "";
                    String postData = "";
                    
                    String inStreamLine;
                    String inStreamLinelc;

                    do{
                        inStreamLine = readLine(in);
                        inStreamLinelc = inStreamLine.toLowerCase();
                        inStream += inStreamLine + "\n";


                        /*
                        User-Agent: Mozilla/5.0 (X11; Linux i686; rv:7.0.1) Gecko/20100101 Firefox/7.0.1
                        Accept: text/html,application/xhtml+xml,application/xml;...
                        Accept-Language: en-us,en;q=0.5
                        Accept-Encoding: gzip, deflate
                        Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
                        Connection: keep-alive
                        */

                        if(HTTPMirror.cmdDebugMode){
                            System.out.println("HTTP request: \"" + inStreamLine + "\"");
                        }
                        
                        try{
                        
                            //GET / HTTP/1.1
                            if (inStreamLinelc.startsWith("post") ||
                                inStreamLinelc.startsWith("get") ||
                                inStreamLinelc.startsWith("put")){

                                String[] line = inStreamLine.split(" ");
                                if (line.length >= 1) command = line[0];
                                if (line.length >= 2) querry = line[1];
                                if (line.length >= 3) protocol = line[2].split("/")[0];
                                if (line.length >= 3) protocolVersion = line[2].split("/")[1];
                            }

                        }catch(Exception e){
                            System.err.println("ERROR: Wrong HTTP request: \"" + inStreamLine + "\"");
                        }
                        
                        //Host: localhost:80
                        if (inStreamLinelc.startsWith("host:")){
                            host = inStreamLine.split(":")[1].trim();
                            serverPort = (inStreamLine.split(":").length >=3) ? Integer.valueOf(inStreamLine.split(":")[2].trim()) : 80;
                        }

                        if (inStreamLinelc.startsWith("post")) post = true;
                        
//                        Content-Type: multipart/form-data; boundary=---------------------------491299511942";
                        
                        //post
                        if(post && inStreamLine.toLowerCase().contains("content-length")){
                           String contentLengthLine = inStreamLine.substring(inStreamLinelc.indexOf("content-length"));
                           contentlenght = Integer.valueOf(contentLengthLine.replaceAll(" ", "").substring(contentLengthLine.indexOf(":") + 1));
                           

                        }else if (post && inStreamLine.toLowerCase().contains("content-type")){
                        	String contentTypeLine = inStreamLine.substring(inStreamLinelc.indexOf("content-type"));
                        	contentTypeLine = contentTypeLine.replaceAll(" ", "").substring(contentTypeLine.indexOf(":") + 1);
                        	
                        	if (contentTypeLine.contains(";") && contentTypeLine.toLowerCase().contains("boundary")){
                        		contenType = contentTypeLine.split(";")[0].trim();
                        		
                        		if (contentTypeLine.toLowerCase().contains("boundary")){
                        			String boundaryLine = contentTypeLine.toLowerCase().split(";")[1];
                        			boundary = boundaryLine.split("=")[1].trim();
                        		}
                        	}
                        }

                    }while(!inStreamLine.isEmpty());

                    URL url = new URL(protocol, host, urlPort, querry);

//                    if(post){
//                        int postChar = 0;
//
//                        for (int postContentIndex = 0; postContentIndex < contentlenght; postContentIndex++) {           
//                            postChar = in.read();
//                            postData += (char)postChar;
//                        }
//                    }

                    if (debug) System.out.println(inStream);

                    out = new PrintWriter(clientsocket.getOutputStream());

                    if (debug || (url.getQuery() != null && url.getQuery().startsWith("info"))){
                    	
                    	out.print("HTTP/1.1 200 OK\n");
                        out.print("Content-Type: text/html\n\n");
                        out.print("<b>" + HTTPMirror.appFullVersionString + "</b><br>");
                        out.print("<br>Views: " + server.getViews() + "<br>");
                        out.print("Connections: " + server.getCurrentConnectionsCount() + "<br>");

                        out.print("<br>");
                        out.print("<form action=\"\" method=\"POST\">");
                        out.print("<table><tr><td><input type=\"text\" name=\"input\" value=\"Hallo :D\"/></td>");
                        out.print("<td><input type=\"submit\"/></td></tr></table></form>");

                        out.print("<a href=\"/?dir\">View directorys >>></a><br>");                        

                        out.print("<hr></hr>");
                        out.print("<br>Quarry: " + url.getQuery());
                        if (post) out.print("<br>PostData: " + postData.replace("\n", "<br>"));

                        out.print("<hr></hr>");
                        out.print("<br><b>connection: " + connection + "</b><br>");
                        out.print("<br>" + inStream.replaceAll("\n", "<br>"));

                    }else if (url.getQuery() != null && url.getQuery().startsWith("dir")){

                        out.print(getDiectoryPage(new File(".")));
                        
                    }else if (command.equalsIgnoreCase("get")){

                        String path = URLDecoder.decode(url.getFile(), "UTF-8");
                        
                        if(path.startsWith("/")) path = path.substring(1);
                        if(path.isEmpty()) path = "index.html";

                        //remove qurry
                        if (url.getQuery() != null) path = path.replace("?" + url.getQuery(), "");
                        
                        File file = new File(path);

                        if (file.exists()){
                            out.print(getDiectoryPage(file));
                            providFile(file, clientsocket);
                        }else{
                            out.print(getError404Page(url.getFile()));
                        }
                    }else if (command.equalsIgnoreCase("post")){

//                        String path = URLDecoder.decode(url.getFile(), "UTF-8");
                        
//                        if(path.startsWith("/")) path = path.substring(1);
//                        if(path.isEmpty()) path = "index.html";
                        
                        //remove qurry
//                        if (url.getQuery() != null) path = path.replace("?" + url.getQuery(), "");
                        
//                        File file = new File(path);

//                        if (file.exists()){
//                            out.print(getDiectoryPage(file));
                            recieveFile(contentlenght, contenType, boundary, in, out);
//                        }else{
//                            out.print(getError404Page(url.getFile()));
//                        }
                    }

                    out.close();
                    in.close();
                    clientsocket.close();
                }   

            }catch (Exception e) {
                this.error = e;
                stopedWithError = true;
                try{
                	if (debug) {
                		e.printStackTrace();
                		printError500Page(e, out);
                	}else{
                		out.print(getError500Page(e));
                	}
                }catch(Exception ex){
                	System.err.print("500: Internal Server Error: couldn't show the error page, usual error was: "); e.printStackTrace();
                }
            }finally{
            	try{
            	 out.close();
                 in.close();
                 clientsocket.close();
            	}catch(Exception exe){
            		exe.printStackTrace();
            	}
            }
            
            server.clients.remove(this);
        }

        @Override
        public void start(){
            super.start();
        }

        public boolean hasStopedWithError(){
            return stopedWithError;
        }

        public Exception getError(){
            return error;
        }
        
        private String getError404Page(String path){
            return  "HTTP/1.1 200 OK\n" +
            		"Content-Type: text/html\n\n" + 
                    "<b>" + HTTPMirror.appFullVersionString + "</b><br>" +
                    "<br><b>Error:</b> 404 file not found! :(<br><br>" + path;
        }
        
        private void printError500Page(Exception e, PrintWriter s){
	       try{        
                s.write("HTTP/1.1 200 OK\n" +
                        "Content-Type: text/html\n\n" + 
                        "<b>" + HTTPMirror.appFullVersionString + "</b><br>" +
                        "<br><b>Error: 500 Internal Server Error</b><br>" +
                        "<br><p><font color=\"#FF0000\">");
                e.printStackTrace(s);
                s.write("</font></p>");  
           }catch(Exception ess){
           }
        }
        
        private String getError500Page(Exception e){
            return  "HTTP/1.1 200 OK\n" +
	        		"Content-Type: text/html\n\n" + 
                    "<b>" + HTTPMirror.appFullVersionString + "</b><br>" +
                    "<br><b>Error: 500 Internal Server Error</b><br>" +
                    "<br><p><font color=\"#FF0000\">" + e.toString() + "</font></p>";
        }

        private String getDiectoryPage(File file) throws Exception{
            if (!file.isDirectory()) return "";

            String path = file.getPath().replaceAll("\\\\", "/"); //replace \ with /
            String back = "/" + path.replace(file.getName(), "");
            if (back.equals("/")) back = "/?dir";
            if (file.getName().equals(".")) back = "/";
            
            File[] filesUnsorted = file.listFiles();
            
            ArrayList<File> dirs = new ArrayList<File>();
            ArrayList<File> files = new ArrayList<File>();
            
            for (File curfile : filesUnsorted) {
				if (curfile.isDirectory()) dirs.add(curfile);
				else files.add(curfile);
			}
            
            String titlepath = path;
            if (titlepath.equals(".")) titlepath = "";
            String title = "Index of /" + titlepath;
            
            String page = "HTTP/1.1 200 OK\n";
            			page += "Content-Type: text/html\n\n";

			            page += "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n";
	            		page += "<html>\n";
	            		page += "<head><title>" + title + "</title></head>\n";
	            		page += "<body>\n";
	            		page += "<h1>" + title +"</h1>\n";
	            		page += "<ul>\n";
	            		page += "<li><a href=\"" + back + "\">..</a></li>\n";

			 
    		for (File curentDir : dirs) {
                String filePath = "/" + curentDir.getPath().replaceAll("\\\\", "/"); //replace \ with /
                String fileName = curentDir.getName();

                page += "<li><a href=\"" + filePath +"\"> " + fileName + "</a></li>\n";
            }
            for (File curentFile : files) {
                String filePath = "/" + curentFile.getPath().replaceAll("\\\\", "/"); //replace \ with /
                String fileName = curentFile.getName();

                page += "<li><a href=\"" + filePath +"\"> " + fileName + "</a></li>\n";
            }
            
    		page += "</ul>\n";
    		page += "<address>" + getServerName() + " Server at " + getServerAddress() + " Port " + getServerPort() + "</address>\n";
    		page += "</body></html>\n";

            return page;
        }

        private boolean recieveFile(int contentlenght, String contenType, String boundary, InputStream in, PrintWriter out) throws Exception{
        	        	
//            InputStream in = socket.getInputStream();
            
//            MimetypesFileTypeMap mime = new MimetypesFileTypeMap();
//         
//            //add sine mime types
//            mime.addMimeTypes("application/java-archive jar");
//            mime.addMimeTypes("application/java-serialized-object ser");
//            mime.addMimeTypes("application/java-vm class");
//            mime.addMimeTypes("application/pdf pdf");
//            mime.addMimeTypes("application/zip zip");
//            ,
//            mime.addMimeTypes("audio/x-wav wav");
//            mime.addMimeTypes("audio/midi midi mid");
//            mime.addMimeTypes("image/gif gif");
//            mime.addMimeTypes("image/jpeg jpeg jpg jpe jfif pjpeg pjp");
//            mime.addMimeTypes("image/png png");
//            mime.addMimeTypes("image/tiff tiff tif");
//            mime.addMimeTypes("image/bmp bmp");
//            mime.addMimeTypes("text/html htm html");
//            mime.addMimeTypes("text/plain txt");
//            mime.addMimeTypes("text/richtext rtx");
//            mime.addMimeTypes("text/tab-separated-values tsv");
//            mime.addMimeTypes("text/x-setext etx");
//            mime.addMimeTypes("text/x-speech talk");
//            mime.addMimeTypes("video/isivideo fvi");
//            mime.addMimeTypes("video/mpeg mpeg mpg mpe mpv vbs mpegv");
//            mime.addMimeTypes("video/x-mpeg2 mpv2 mp2v");
//            mime.addMimeTypes("video/msvideo avi");
//            mime.addMimeTypes("video/quicktime qt mov moov");
//            mime.addMimeTypes("video/vivo viv vivo");
//            mime.addMimeTypes("video/wavelet wv");
//            mime.addMimeTypes("video/x-sgi-movie movie");


        	String filename = "file.dat";
        	
            String dataPart1 = "";
            String line = "";         
            long bytesRead = 0;
           
            while (!dataPart1.endsWith("" + ((char)13) + ((char)10) + ((char)13) + ((char)10) + "")){ 
        	   line += (char)in.read();
        	   bytesRead++;
        	          	   
               if(line.endsWith("\n")){
               	dataPart1 += line;
               	
               	if (line.toLowerCase().startsWith("content-disposition")){
               		if (line.toLowerCase().contains("filename=")){
               			filename = line.split("filename=")[1];
               			filename = filename.substring(1);
               			filename = filename.substring(0, filename.indexOf("\""));
               		}
               	}
               	
               	
               	line = "";
               }
            }
            
//            System.out.println("\"" + dataPart1 + "\"");
            
//            Content-Disposition: form-data; name="file"; filename="Ich bin Nummer 4.avi"
            
            
            
            
            
//            System.out.println(contentlenght + " - " + bytesRead + " - " + (boundary + "--\n").length() + " = " + (contentlenght - bytesRead - boundary.length()));
        	int offsetBytes = 8;
            long filesize = contentlenght - bytesRead - boundary.length() - offsetBytes;
            FileTransfare fileTransfere = new FileTransfare(filename, filesize, in, out);
            fileTransfere.start();

            while (fileTransfere.isRunning()){
            	Thread.sleep(1000);
            }
      
            return fileTransfere.wasSuccessfull();
        }
        
        private boolean providFile(File file, Socket socket) throws Exception{
            if (file.isDirectory()) return false;

            out = new PrintWriter(socket.getOutputStream());
          
            
            String page = "HTTP/1.1 200 OK\n"+
            				//"Accept-Ranges: bytes\n" +
				            //"Content-transfer-encoding: binary\n" +
				            "Content-Length: " + file.length() + "\n" +
				            //"Content-Disposition: attachment; filename=\""+ file.getName() + "\"\n" +
				            "Content-Type: " +  mime.getContentType(file) + "\n\n";
 
            out.print(page);
            out.flush();
            
            FileTransfare fileTransfere = new FileTransfare(file, socket);
            fileTransfere.start();

            while (fileTransfere.isRunning()){
            	Thread.sleep(1000);
            }
      
            return fileTransfere.wasSuccessfull();
        }
   
		class FileTransfare extends Thread {

			public static final int TRANSFARE_MODE_PROVIDE = 0;
			public static final int TRANSFARE_MODE_RECIEVE = 1;
			
			private int transfaremode;
			private File file;
			private Socket socket;
			
			//recieve
			private String filename;
			private InputStream in;
			private long contentlenght;
			private PrintWriter out;

			boolean running = true;
			boolean successfull = false;
			
			final int BUFFER_SIZE = 65536;

			public FileTransfare(File file, Socket socket) {
				this.transfaremode = TRANSFARE_MODE_PROVIDE;
				this.file = file;
				this.socket = socket;
			}
			
			public FileTransfare(String filename, long contentlenght, InputStream in, PrintWriter out) {
				this.filename = filename;
				this.transfaremode = TRANSFARE_MODE_RECIEVE;
				this.contentlenght = contentlenght;
				this.in = in;
				this.out = out;
			}
	
			public void run() {
				if (this.transfaremode == TRANSFARE_MODE_PROVIDE){
					provide();
				}else if(this.transfaremode == TRANSFARE_MODE_RECIEVE){
					recieve();
				}
			}
			
			public void recieve() {
				
				FileOutputStream fileOutputStream = null;
				InputStream socketInputStream = null;
				
				try {
					running = true;
					
					file = new File(filename);
					
					System.out.printf("Recieving file: %s bytes %s\n", contentlenght, file.getAbsolutePath());
					
					fileOutputStream = new FileOutputStream(file.getAbsolutePath());
					socketInputStream = in; //socket.getInputStream();

					long startTime = System.currentTimeMillis();

					byte[] buffer = new byte[BUFFER_SIZE];
					if (BUFFER_SIZE > contentlenght) buffer = new byte[(int) contentlenght];
					
					long lenght = contentlenght;
					long statusTime = startTime;
					
					long readTotal = 0L;
					int read = 0;
					
					while (readTotal < lenght) {
						read = socketInputStream.read(buffer);
						fileOutputStream.write(buffer, 0, read);
						readTotal += read;
						
						if (System.currentTimeMillis() - statusTime >= 10000) {
							double mbps = ((readTotal / 1024.0) / ((System.currentTimeMillis() - startTime) / 1000.0)) / 1024.0;
							System.out.printf("Transfered: %s%% %s bytes -> %s Mbytes/s\n", Math.round(readTotal / ((double) lenght) * 100.0), readTotal, mbps);
							
							out.print("HTTP/1.1 200 OK\nContent-Type: text/html\n\n" +
									"<br><b>Transfered: </b>" + Math.round(readTotal / ((double) lenght) * 100.0) + "% " + readTotal + " bytes <b>-></b> " + mbps + " Mbytes/s\n");
							
							statusTime = System.currentTimeMillis();
						}
						
						if (readTotal > lenght) break;
					}
					successfull = true;
					
					socketInputStream.close();
					fileOutputStream.close();

					long endTime = System.currentTimeMillis();
					double time = (endTime - startTime) / 1000.0;
	
					System.out.printf("Transfered: %s bytes in: %s s -> %s Mbytes/s\n", readTotal, time, ((readTotal / 1024.0) / time) / 1024.0);
				} catch (Exception e) {
					if (!successfull && isRunning) System.out.println("Aborted file providing: " + e.getLocalizedMessage());
				}finally{
					try {
						socketInputStream.close();
						fileOutputStream.close();
					} catch (Exception e) {}
				}
				
				running = false;
			}
			
			public void provide() {
				FileInputStream fileInputStream = null;
				OutputStream socketOutputStream = null;
				
				try {
					running = true;
					
					System.out.printf("Providing file: %s bytes %s\n", file.length(), file.getAbsolutePath());
					
					fileInputStream = new FileInputStream(file.getAbsolutePath());
					socketOutputStream = socket.getOutputStream();

					long startTime = System.currentTimeMillis();

					byte[] buffer = new byte[BUFFER_SIZE];
					
					long lenght = file.length();
					long statusTime = startTime;
					
					long readTotal = 0L;
					int read;
					
					while (readTotal < lenght) {
						read = fileInputStream.read(buffer);
						socketOutputStream.write(buffer, 0, read);
						readTotal += read;
						
						if (System.currentTimeMillis() - statusTime >= 10000) {
							double mbps = ((readTotal / 1024.0) / ((System.currentTimeMillis() - startTime) / 1000.0)) / 1024.0;
							System.out.printf("Transfered: %s%% %s bytes -> %s Mbytes/s\n", Math.round(readTotal / ((double) lenght) * 100.0), readTotal, mbps);
							statusTime = System.currentTimeMillis();
						}
					}
					successfull = true;
					
					socketOutputStream.close();
					fileInputStream.close();

					long endTime = System.currentTimeMillis();
					double time = (endTime - startTime) / 1000.0;
	
					System.out.printf("Transfered: %s bytes in: %s s -> %s Mbytes/s\n", readTotal, time, ((readTotal / 1024.0) / time) / 1024.0);
				} catch (Exception e) {
					if (!successfull && isRunning) System.out.println("Aborted file providing: " + e.getLocalizedMessage());
				}finally{
					try {
						socketOutputStream.close();
						fileInputStream.close();
					} catch (Exception e) {}
				}
				
				running = false;
			}
			
			public boolean wasSuccessfull() {
				return successfull;
			}
			public boolean isRunning(){
				return running;
			}
		}
		   	  
    }
    
}

